// Silnia.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cstdlib>

class Silnia {

public:
    int iteration(int n) {
        int result = n;
        for (int i = 1; i < n; i++) {
            result *= i;
        }
        return result;
    }

public:
    int recuration(int n) {
        if (n < 2) {
            return 1;
        }
        return n * recuration(n - 1);
    }
};

class Fibonaccie {

public:
    int iteration(int n) {
        int a, b;
        if (n == 0) return 0;

        a = 0; b = 1;
        for (int i = 0; i < (n - 1); i++)
        {
            std::swap(a, b);
            b += a;
        }
        return b;
    }

public:
    int recuration(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return recuration(n - 1) + recuration(n - 2);
    }
};

int main()
{

    std::string number;
    int i = std::stoi(number);


    std::cout << "Podaj liczbę dla obliczenia silni: " << "\n";
    std::cin >> number;
    int n = atoi(number.c_str());

    Silnia silnia;

    std::cout << "Silnia iteracja: " << silnia.iteration(n) << "\n";
    std::cout << "Silnia rekurencja: " << silnia.recuration(n) << "\n";

    std::cout << "Podaj który wyraz ciągu fibonacciego chcesz zobaczyć: " << "\n";
    std::cin >> number;

    Fibonaccie fibonaccie;
    std::cout << "Fibonaccie iteracja: " << fibonaccie.iteration(n) << "\n";
    std::cout << "Fibonaccie rekurencja: " << fibonaccie.recuration(n) << "\n";
}
